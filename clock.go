package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"time"
)

func TimeIn(t time.Time, name string) (time.Time, error) {
	loc, err := time.LoadLocation(name)
	if err == nil {
		t = t.In(loc)
	}
	return t, err
}

func main() {
	hostname := "localhost"
	if len(os.Args) > 1 {
		hostname = os.Args[1]
	}
	timezone := "UTC"
	if len(os.Args) > 2 {
		timezone = os.Args[2]
	}

	listener, err := net.Listen("tcp", fmt.Sprintf("%s:8000", hostname))
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		go handleConn(conn, timezone)
	}
}

func handleConn(c net.Conn, timezone string) {
	defer c.Close()
	for {
		t, err := TimeIn(time.Now(), timezone)
		if err != nil {
			log.Fatal(err)
			return
		}
		_, err = io.WriteString(c, t.Format("Mon, 02 Jan 2006 15:04:05 MST\n"))
		if err != nil {
			return
		}
		time.Sleep(1 * time.Second)
	}
}
